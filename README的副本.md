# ptengine for App

> App project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
sudo(if mac) gulp

# build for production with minification
gulp build

```
