var pt = {
  'para': '',
  'version': '1.0.0'
};

var _ = {};

pt._queue = [];
// 是否已经获取到系统信息
pt.getSystemInfoComplete = false;

var ArrayProto = Array.prototype,
  FuncProto = Function.prototype,
  ObjProto = Object.prototype,
  slice = ArrayProto.slice,
  toString = ObjProto.toString,
  hasOwnProperty = ObjProto.hasOwnProperty,
  LIB_VERSION = '1.0.0',
  LIB_TYPE = 'wechat',
  Page_ID = '',
  Event_Paras = {},
  ptGetData,
  user_ID = '',
  SessionStartTime,
  SessionId,
  sendInterval = 10 * 1000, // 10s发包机制
  sendPackLimit,
  sessionKeepAlive = 300, // 心跳包时间
  backgroundContinued = 30 * 1000, // 退出后台时间
  loadConfigPeriod = 300 * 1000, // 读取配置时间
  TimeOff = 0, // 与服务器时间做校验
  serverTime,
  eventBegin,
  eventBeginName,
  eventStop,
  keepAliveTimer,
  overAllInter,
  backTimer,
  backSendTimer,
  NetWork,
  Language,
  OsVersion,
  AppVersion,
  SdkVersion,
  Channel,
  ptRef = '0', // 
  setInterNum = 1, //
  TimeZone; // 时区

/**
 * To Array.prototype add methods
 */
ArrayProto.indexOf = function(val) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == val) return i;
  }
  return -1;
};
ArrayProto.remove = function(val) {
  var index = this.indexOf(val);
  if (index > -1) {
    this.splice(index, 1);
  }
};

// 读取配置设备默认值
// var SendingTimeInterval = 10 * 1000,
//   BackgroundContinued = 30 * 1000,
//   MaxPostDataLength = 50 * 1024,
//   LoadConfigPeriod = 300 * 1000,
//   SessionKeepAlive = 300,
//   TimeOff = 0; // 判断本地时间与服务器时间差值

pt.lib_version = LIB_VERSION;

var logger = typeof logger === 'object' ? logger : {};
logger.info = function() {
  if (typeof console === 'object' && console.log) {
    try {
      return console.log.apply(console, arguments);
    } catch (e) {
      console.log(arguments[0]);
    }
  }
};

(function() {
  var nativeBind = FuncProto.bind,
    nativeForEach = ArrayProto.forEach,
    nativeIndexOf = ArrayProto.indexOf,
    nativeIsArray = Array.isArray,
    breaker = {};

  var each = _.each = function(obj, iterator, context) {
    if (obj === null) {
      return false;
    }
    if (nativeForEach && obj.forEach === nativeForEach) {
      obj.forEach(iterator, context);
    } else if (obj.length === +obj.length) {
      for (var i = 0, l = obj.length; i < l; i++) {
        if (i in obj && iterator.call(context, obj[i], i, obj) === breaker) {
          return false;
        }
      }
    } else {
      for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) {
          if (iterator.call(context, obj[key], key, obj) === breaker) {
            return false;
          }
        }
      }
    }
  };
  _.logger = logger;
  //
  _.extend = function(obj) {
    each(slice.call(arguments, 1), function(source) {
      for (var prop in source) {
        if (source[prop] !== void 0) {
          obj[prop] = source[prop];
        }
      }
    });
    return obj;
  };

})();

_.isJSONString = function(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
};
_.isObject = function(obj) {
  return (toString.call(obj) === '[object Object]') && (obj !== null);
};
_.isEmptyObject = function(obj) {
  if (_.isObject(obj)) {
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) {
        return false;
      }
    }
    return true;
  }
  return false;
};
_.isString = function(obj) {
  return toString.call(obj) === '[object String]';
};
_.isDate = function(obj) {
  return toString.call(obj) === '[object Date]';
};
_.isBoolean = function(obj) {
  return toString.call(obj) === '[object Boolean]';
};
_.isNumber = function(obj) {
  return (toString.call(obj) === '[object Number]' && /[\d\.]+/.test(String(obj)));
};

_.getAllData = function(callback) {

  var _diffDate = new Date().getTimezoneOffset();
  if (_diffDate < 0) {
    TimeZone = 'GMT+' + (-_diffDate / 60);
  } else if (_diffDate > 0) {
    TimeZone = 'GMT-' + _diffDate / 60;
  } else {
    TimeZone = 'GMT+' + 0;
  };

  var _getNetWork = function() {
    wx.getNetworkType({
      'success': function(t) {
        NetWork = t['networkType'];
      },
      'complete': _getSystemInfo
    })
  };
  // 判断设备
  var _getSystemInfo = function() {
    wx.getSystemInfo({
      'success': function(t) {
        SdkVersion = String(LIB_VERSION);
        AppVersion = pt.para.appVersion;
        OsVersion = t.system.split(' ')[1];
        Language = t['language'];
        if (callback) {
          callback();
        }
      }
    });
  };
  _getNetWork();
};
_.getConfig = function(callback) {
  wx.request({
    url: 'https://appconfigloader.ptengine.cn/app/appConfig?appId=5d1rklk88q',
    method: 'get',
    header: {
      'content-type': 'application/json;charset=UTF-8;' // 默认值
    },
    fail: function() {
      console.log('获取配置错误！');
    },
    success: function(res) {
      var _data = JSON.parse(res.data.content);

      sendInterval = _data.sendInterval; // 获取配置每10s发包
      backgroundContinued = _data.backgroundContinued; // 获取超时
      serverTime = _data.serverTime;
      loadConfigPeriod = _data.loadConfigPeriod;
      sessionKeepAlive = _data.sessionKeepAlive;

      TimeOff = +new Date() - serverTime;

      if (callback) {
        callback();
      }

    }
  });
};
_.getConfig();
// _.getTimeZone = function() {
// var _diffDate = new Date().getTimezoneOffset();
// if (_diffDate < 0) {
//   TimeZone = 'GMT+' + (-_diffDate / 60);
// } else if (_diffDate < 0) {
//   TimeZone = 'GMT-' + _diffDate / 60;
// } else {
//   TimeZone = 'GMT+' + 0;
// }
// };

_.info = {
  properties: {
    packType: LIB_TYPE,
    projectId: pt.para.projectId,
    appId: pt.para.appId,
    packData: []
  },
  getSystem: function() {
    var _e = this.properties,
      _this = this;
    // 判断网络    
    var _getNetWork = function() {
      wx.getNetworkType({
        'success': function(t) {

        },
        'complete': _getSystemInfo
      })
    };
    // 判断设备
    var _getSystemInfo = function() {
      wx.getSystemInfo({
        'success': function(t) {
          _e.deviceModel = t['model'];
          _e.resolution = t.windowWidth + 'x' + t.windowHeight;
          _e.os = t.system.split(' ')[0];
          _e.deviceBranding = t['brand'];
          _e.wechatVersion = t['version'];
          _e.MPBaseSDK = t.SDKVersion;
        },
        'complete': _this.setStatusComplete
      });
    };
    _getNetWork();
    // 

  },
  setStatusComplete: function() {
    pt.getSystemInfoComplete = true; // 设置为true
    if (pt._queue.length > 0) {
      _.each(pt._queue, function(cxt) {
        pt.prepareData.apply(pt, slice.call(cxt));
      });
      pt._queue = [];
    }
  }
};

pt._ = _;

// 处理存放所有属性
pt.prepareData = function(p, callback) {
  if (!pt.getSystemInfoComplete) {
    pt._queue.push(arguments);
    return false;
  }

  // pt.store.getDistinctId();
  var data = {
    deviceId: this.store.getDistinctId(),
    androidId: this.store.getDistinctId(),
    packTime: +new Date() - TimeOff,
    packId: '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8),
    properties: {}
  };

  var _packData = _.info.properties.packData;
  _packData.push(p);
  data.properties = _.extend({}, _.info.properties);


  wx.setStorageSync('pt_temp_data', data);

  if (callback) {
    callback();
  }

};

/**
 * pt.store 底层相关方法
 */
pt.store = {
  _state: {},
  getUUID: function() {
    return '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
  },
  setStorage: function() {

  },
  getStorage: function() {
    return wx.getStorageSync('ptapp2018_wechat') || '';
  },
  getFirstId: function() {
    return this._state.first_id;
  },
  getDistinctId: function() {
    return this._state.deviceId;
  },
  set: function(name, value) {
    var _obj = {};
    if (typeof name === 'string') {
      _obj[name] = value;
    } else if (typeof name === 'object') {
      _obj = name;
    }
    this._state = this._state || {};
    for (var i in _obj) {
      this._state[i] = _obj[i];
    }
    this.save();
  },
  save: function() {
    wx.setStorageSync('ptapp2018_wechat', JSON.stringify(this._state));
  },
  toState: function(d) {
    var _sta = null;
    if (_.isJSONString(d)) {
      _sta = JSON.parse(d);
      console.log('_sta.deviceId' + _sta.deviceId);
      if (_sta.deviceId) {
        this._state = _sta;
        console.log('this._state:' + JSON.stringify(this._state));
      } else {
        this.set('deviceId', this.getUUID());
      }
    } else {
      this.set('deviceId', this.getUUID());
    }
  },
  init: function() {
    var _info = this.getStorage();
    console.log('_info:' + _info);
    if (_info) {
      this.toState(_info);
    } else {
      var _time = (new Date()),
        _visit_time = _time.getTime();
      _time.setHours(23);
      _time.setMinutes(59);
      _time.setSeconds(60);
      this.set({
        'deviceId': this.getUUID()
          // 'firstVisitTime': _visit_time,
          // 'firstVisitDayTime': _time.getTime()
      });
    }

  },
  getSystemError: function(c, err) { // 判断系统以及自定义错误
    switch (c) {
      case 0:
        var _resInfo = wx.getStorageInfoSync();
        if (_resInfo.currentSize > _resInfo.limitSize) {
          pt.tracking('systemError', {
            'systemErrorInfo': err
          })
        }
    }
  }
};

console.log('存储' + pt.store.getStorage());



// pt.setProfile = function(p, c) {
//   pt.prepareData({
//     type: 'profile_set',
//     properties: p
//   }, c);
// };

// pt.setOnceProfile = function(p, c) {
//   pt.prepareData({
//     type: 'profile_set_once',
//     properties: p
//   }, c);
// };

// 自定义追踪事件--高级埋码
pt.tracking = function(e, p, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'advancedEvent',
    eventType: 'custom',
    timestamp: +new Date() - TimeOff,
    sessionStartTime: SessionStartTime,
    sessionId: SessionId,
    eventName: e,
    eventParas: p,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
};


// 错误捕捉--
pt.trackError = function(code, info, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'errorInfo',
    timestamp: +new Date() - TimeOff,
    sessionStartTime: SessionStartTime,
    sessionId: SessionId,
    errorInfo: info,
    errorCode: code,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
}

// 心跳包事件
pt.sendKeepAlive = function(c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'advancedEvent',
    eventType: 'custom',
    timestamp: +new Date() - TimeOff,
    sessionStartTime: SessionStartTime,
    sessionId: SessionId,
    eventName: 'pt_session_keepalive',
    eventParas: [],
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  // });
};

// 事件停留时长--begin
pt.getBegin = function(n) {
  eventBegin = +new Date() - TimeOff;
  eventBeginName = n;
  setInterNum = 1;
};

// 事件停留时长--stop
pt.getStop = function(e, p, c) {
  eventStop = +new Date() - TimeOff;
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'advancedEvent',
    eventType: 'custom',
    timestamp: +new Date() - TimeOff,
    sessionStartTime: SessionStartTime,
    sessionId: SessionId,
    eventName: e,
    eventParas: p,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone,
    pt_durations: String(Math.floor((eventStop - eventBegin) / 1000))
  }, c);
  setInterNum = 1;
  // });
};


// 全局默认事件
pt.track = function(e, p, c) {
  // _.getTimeZone();

  var _setInter = setInterval(function() {
    if (SessionStartTime !== undefined && SessionId !== undefined) {
      clearInterval(_setInter);

      // pt._.getAllData(function() {
      console.log('时间SessionStartTime:' + SessionStartTime)
      pt.prepareData({
        category: 'pv',
        pageId: Page_ID,
        pageName: Page_ID,
        sessionId: SessionId,
        sessionStartTime: SessionStartTime,
        timestamp: +new Date() + 100 - TimeOff,
        network: NetWork,
        language: Language,
        osVersion: OsVersion,
        appVersion: AppVersion,
        sdkVersion: SdkVersion,
        channel: Channel,
        timezone: TimeZone
      }, c);
      setInterNum = 1;
      // });
    }
  }, 100);

};

// 设置pageName事件
pt.setPage = function(n, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'pv',
    pageId: Page_ID,
    pageName: n,
    sessionId: SessionId,
    sessionStartTime: SessionStartTime,
    timestamp: +new Date() - TimeOff,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
};


// 登录判断
pt.trackSignup = function(id, e, p, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'login',
    timestamp: +new Date() - TimeOff,
    sessionId: SessionId,
    sessionStartTime: SessionStartTime,
    uid: id,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
  // pt.store.set('deviceId', id);
};

// loadConfigPeriod 读取配置时间
// serverTime 读取服务器时间
// backgroundContinued 超过30s，重新启动
// sessionKeepAlive 每次有事件入库的时候，需要重新启动---

/**
 * 拉取配置，
 * 发包的时候对包进行校验大小，拆包；拆包直至符合大小策略；
 * 
 * 进入后台发送就发送所有的包，并且把keepAlive干掉；进入后台后又进入到前台超过30s，重新计算sessionStart；重新启动心跳包
 * 
 */

// 启动包
pt.startUp = function(c) {
  console.log('TimeOff:' + TimeOff);
  // SessionStartTime = new Date().getTime();
  // SessionStartTime = +new Date() - TimeOff;
  // console.log('SessionStartTime:' + SessionStartTime);
  // SessionId = '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
  // // _.getTimeZone();
  if (ptRef !== '0') {
    _.info.properties.shareRef = ptRef;
  }
  console.log('_.info.properties.ptShare:' + ptRef);
  pt._.getAllData(function() {
    pt.prepareData({
      category: 'sessionStart',
      timestamp: SessionStartTime,
      sessionId: SessionId,
      network: NetWork,
      language: Language,
      osVersion: OsVersion,
      appVersion: AppVersion,
      sdkVersion: SdkVersion,
      channel: Channel,
      timezone: TimeZone
    }, c);
    setInterNum = 1;
  });
};

// 页面停留时长
pt.getPageTime = function(t, p, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'pageDuration',
    timestamp: +new Date() - TimeOff,
    sessionId: SessionId,
    pt_page_durations: t,
    pageId: p,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
};


// 默认分享的时候用
pt.share = function(e, p, c) {
  // _.getTimeZone();
  // pt._.getAllData(function() {
  pt.prepareData({
    category: 'advancedEvent',
    eventType: 'custom',
    timestamp: +new Date() - TimeOff,
    sessionStartTime: SessionStartTime,
    sessionId: SessionId,
    eventName: e,
    eventParas: p,
    network: NetWork,
    language: Language,
    osVersion: OsVersion,
    appVersion: AppVersion,
    sdkVersion: SdkVersion,
    channel: Channel,
    timezone: TimeZone
  }, c);
  setInterNum = 1;
  // });
};


pt.publicAttr = function() {


};

pt.login = function(id) {
  var _firstId = pt.store.getFirstId(),
    _distinctId = pt.store.getDistinctId();


  if (id !== _distinctId) {
    if (_firstId) {
      pt.trackSignup(id);
    } else {
      // pt.store.set('first_id', _distinctId);
      pt.trackSignup(id);
    }
  }
  setInterNum = 1;
};


pt.init = function() {
  // this._.info.getUserInfo();
  this._.info.getSystem();
  this.store.init();

};

pt.send = function(t) {
  var o = 0;
  var url = '';

  // logger.info(t);
  _.extend(t, t.properties);

  delete t.properties;

  logger.info(t);
  console.log('长度：' + (encodeURIComponent(JSON.stringify(t)).length / 1024));

  if ((encodeURIComponent(JSON.stringify(t)).length / 1024) > 50) {
    console.log('分包执行');
    var _paraPack = {};
    _.each(t, function(a, i) {
      if (i !== 'packData') {
        _paraPack[i] = a;
      }
    });
    var _paraPackLen = encodeURIComponent(JSON.stringify(_paraPack)).length / 1024;
    // var newArr = [];
    // console.log('ttt:' + JSON.stringify(t));
    _.each(t, function(a, i) {

      // console.log('t[i]:' + JSON.stringify(t));


      if (i === 'packData') {
        var myArr = a;
        var _objs = [];

        // console.log('myArr:' + JSON.stringify(myArr));
        // console.log('packData:' + JSON.stringify(t[i]));

        // 如果单个条中有大于50kb的，直接移除
        for (var j = 0, len = myArr.length; j < len; j++) {
          if (Number(_paraPackLen) + Number(encodeURIComponent(JSON.stringify(myArr[j])).length / 1024) > 50) {
            // console.log(myArr[j]);
            myArr.splice(j, 1);
          }
        }

        // console.log('myArr:' + JSON.stringify(myArr));


        function recursionArr(arr) {　　
          // 临时存放数组
          var _arr = [];
          // 对其进行分组
          var _num = Math.ceil(arr.length / 2);
          for (var i = 0; i < arr.length; i += _num) {
            _arr.push(arr.slice(i, i + _num));
          }
          var _pack = {},
            _obj;
          for (var i = 0; i < _arr.length; i++) {
            _obj = {};
            var _pack = {
              packData: _arr[i]
            };
            if (Number(_paraPackLen) + Number(encodeURIComponent(JSON.stringify(_pack)).length / 1024) > 50) {
              recursionArr(_arr[i]);
            } else {
              _.each(t, function(a, iu) {
                if (iu === 'packData') {
                  _obj['packData'] = _arr[i];
                } else if (iu === 'packTime') {
                  _obj['packTime'] = +new Date() - TimeOff;
                } else {
                  _obj[iu] = a;
                }
              });
              _objs.push(_obj);
            }
          }
        }
        // console.log('myArr:' + myArr.length);
        recursionArr(myArr);
        wx.setStorageSync('pt_temp_data1', _objs);
        var _ptTempData1 = wx.getStorageSync('pt_temp_data1');
        // console.log('_ptTempData1:' + _ptTempData1);
        for (var i = 0; i < wx.getStorageSync('pt_temp_data1').length; i++) {
          t = JSON.stringify(_ptTempData1[i]);
          console.log(t);
          var sendRequest = function() {
            wx.request({
              url: pt.para.serverUrl + 'collect/app',
              method: 'POST',
              header: {
                'content-type': 'application/json;charset=UTF-8;' // 默认值
              },
              data: encodeURIComponent(t),
              fail: function() {
                console.log('发送错误，重新发送！');
                o < 2 && (o++, sendRequest());
              },
              success: function(res) {
                _ptTempData1.remove(_ptTempData1[i]);
                wx.setStorageSync('pt_temp_data1', _ptTempData1);
                i = 0;
                // console.log(wx.getStorageSync('pt_temp_data1').length);
                if (wx.getStorageSync('pt_temp_data1').length === 1) {
                  wx.removeStorageSync('pt_temp_data1');
                }
                console.log('1分包数据采集发送完毕！');
              }
            });
          };
          sendRequest();
        }
      }
    });

  } else {
    t = JSON.stringify(t);
    console.log(t);

    // wx.getStorageInfo({
    //   success: function(res) {
    //     console.log('res.keys' + res.keys)
    //     console.log('currentSize:' + res.currentSize)
    //     console.log('res.limitSize' + res.limitSize)
    //   }
    // })

    var sendRequest = function() {
      wx.request({
        url: pt.para.serverUrl + 'collect/app',
        method: 'POST',
        header: {
          'content-type': 'application/json;charset=UTF-8;' // 默认值
        },
        data: encodeURIComponent(t),
        fail: function() {
          console.log('发送错误，重新发送！');
          o < 2 && (o++, sendRequest());
        },
        success: function(res) {
          console.log('正常数据采集发送完毕！');
        }
      });
    };
    sendRequest();
  }
};

function e(t, n, o) {
  if (t[n]) {
    var e = t[n];
    t[n] = function(t) {
      return o.call(this, t, n), e.call(this, t)
    }
  } else
    t[n] = function(t) {
      return o.call(this, t, n)
    }
}

function appLaunch(options) {

  // 
  if (wx.getStorageSync('pt_data_info')) {
    _.info.properties.openid = wx.getStorageSync('pt_data_info')[0].openid;
  } else {
    wx.login({
      success: function(res) {
        if (res.code) {
          console.log('res.code:' + res.code);
          //发起网络请求
          wx.request({
            url: 'https://tsmp.ptengine.cn/mpserver/getOpenID?miniAppId=' + pt.para.miniAppId + '&miniSecret=' + pt.para.miniSecret + '&jsCode=' + res.code,
            header: {
              'content-type': 'application/json;charset=UTF-8;' // 默认值
            },
            method: 'get',
            fail: function() {},
            success: function(res) {
              console.log('返回的值：' + JSON.stringify(res.data));
              _.info.properties.openid = res.data.openid;
              wx.setStorageSync('pt_data_info', [{ openid: res.data.openid }, { sessionKey: res.data.session_key }]);
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    });
  }


  // wx.getLocation({
  //   type: 'wgs84',
  //   success: function(res) {
  //     var latitude = res.latitude;
  //     var longitude = res.longitude;
  //     var speed = res.speed;
  //     var accuracy = res.accuracy;

  //     console.log('latitude:' + latitude);
  //     console.log('longitude:' + longitude);
  //     console.log('speed:' + speed);
  //     console.log('accuracy:' + accuracy);
  //   }
  // })

  // 处理用户授权
  if (wx.getStorageSync('pt_user_info')) {
    _.info.properties.nickName = wx.getStorageSync('pt_user_info')[0];
    _.info.properties.gender = wx.getStorageSync('pt_user_info')[1];
    _.info.properties.avatarUrl = wx.getStorageSync('pt_user_info')[2];
  } else {
    var _ss = setInterval(function() {
      // console.log('在查授权情况');
      wx.getSetting({
        success: function(res) {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称
            wx.getUserInfo({
              success: function(res) {
                var _userInfo = res.userInfo,
                  _nickName = _userInfo.nickName,
                  _avatarUrl = _userInfo.avatarUrl,
                  _gender = _userInfo.gender; //性别 0：未知、1：男、2：女
                _.info.properties.nickName = _nickName;
                _.info.properties.gender = _gender;
                _.info.properties.avatarUrl = _avatarUrl;
                // pt.init();
                var _wechatUser = [_nickName, _gender, _avatarUrl];
                wx.setStorageSync('pt_user_info', _wechatUser);
                console.log('已经授权了');
                clearInterval(_ss);
              }
            })
          }
        }
      });
    }, 1000);
  }


  wx.removeStorageSync('outTime');

  // 获取场景值
  // _.info.properties.channel = String(options.scene);
  Channel = String(options.scene);

  // console.log('公众号：' + JSON.stringify(options));

  // console.log('值：' + wx.getStorageSync('pt_temp_data'));


  var _this = this;

  _this[pt.para.proName] = pt;
  //   pt.prepareDate(2, 5);


  pt.init();


  // 每隔loadConfigPeriod重新刷新下配置
  setInterval(function() {
    pt._.getConfig();
  }, loadConfigPeriod);

  pt._.getConfig(function() {
    SessionStartTime = +new Date() - TimeOff;
    SessionId = '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);

    pt.startUp(function() {
      // console.log('第一启动applanch')
      if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
        wx.getNetworkType({
          success: function(res) {
            var _network = res.networkType;
            // 无网络先把包存起来
            if (_network === 'none') {
              // wx.setStorageSync('pt_temp_data', data);
            } else {
              // 有网络
              // 先判断有没有存储
              pt.store.getSystemError(0, '存储超过系统最大值');
              pt.send(wx.getStorageSync('pt_temp_data'));
              _.info.properties.packData = [];
              wx.removeStorageSync('pt_temp_data');
              console.log('先发一次sessionStart');
            }
          }
        });
      }
    });


  });

};


function appShow(ops) {


  console.log('appshow参数：' + JSON.stringify(ops));


  wx.removeStorageSync('pt_page_time');

  var _options = ops,
    _query = ops.query;

  ptRef = _query.pt_ref ? _query.pt_ref : '0';


  pt.init();
  // 

  if (wx.getStorageSync('outTime')) {
    console.log('退出时间：' + Math.floor((+new Date() - wx.getStorageSync('outTime')) / 1000));
    if (Math.floor((+new Date() - wx.getStorageSync('outTime')) / 1000) > 30) {
      SessionStartTime = +new Date() - TimeOff;
      SessionId = '' + Date.now() + '-' + Math.floor(1e7 * Math.random()) + '-' + Math.random().toString(16).replace('.', '') + '-' + String(Math.random() * 31242).replace('.', '').slice(0, 8);
      pt.startUp(function() {
        // console.log('触发onShow中的startUp');
        if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
          wx.getNetworkType({
            success: function(res) {
              var _network = res.networkType;
              // 无网络先把包存起来
              if (_network === 'none') {
                // wx.setStorageSync('pt_temp_data', data);
              } else {
                // 有网络
                // 先判断有没有存储
                pt.store.getSystemError(0, '存储超过系统最大值');
                pt.send(wx.getStorageSync('pt_temp_data'));
                _.info.properties.packData = [];
                wx.removeStorageSync('pt_temp_data');
                console.log('先发一次sessionStart');
              }
            }
          });
        }
      });
    }
  }

  // clearInterval(backTimer);
  clearInterval(backSendTimer);

  // to do
  var _shareTicket;
  if (ops.scene === 1044) {
    _shareTicket = ops.shareTicket;


  }


  /**
   * @keepAliveTimer 心跳包计时器
   */
  setInterNum = 1;
  keepAliveTimer = setInterval(function() {
    setInterNum++;
    // console.log('setInterNum:' + setInterNum);
    if (setInterNum === (sessionKeepAlive)) {
      pt.sendKeepAlive();
      setInterNum = 1;
    }
  }, 1000);

  var _a = 1;
  overAllInter = setInterval(function() {
    // console.log(sendInterval);
    _a++;
    if (_a === sendInterval / 100) { // 10s发包
      _a = 1;
      // console.log('长度：' + wx.getStorageSync('pt_temp_data').properties.packData.length);
      if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
        wx.getNetworkType({
          success: function(res) {
            var _network = res.networkType;
            // 无网络先把包存起来
            console.log('_newwork:' + JSON.stringify(res));
            if (_network === 'none') {
              // wx.setStorageSync('pt_temp_data', data);
              // console.log('网络断开了！');
            } else {
              // 有网络
              // 先判断有没有存储
              pt.store.getSystemError(0, '存储超过系统最大值');
              pt.send(wx.getStorageSync('pt_temp_data'));
              _.info.properties.packData = [];
              wx.removeStorageSync('pt_temp_data');
              console.log('10s一发');
            }
          }
        });
      }
    }
    // console.log(_a);
  }, 100);




};

function appHide(n, e) {

  // 切换到后台的时候，清空心跳包
  clearInterval(keepAliveTimer);
  // 退出后台的时候暂停10s发包机制
  clearInterval(overAllInter);


  // 退出的时候，先结束下页面停留时长
  var _leveTime = new Date().getTime() - wx.getStorageSync('pt_page_time').ptTime,
    _page = wx.getStorageSync('pt_page_time').ptPage;

  pt.getPageTime(Math.floor(_leveTime / 1000), _page);
  wx.removeStorageSync('pt_page_time'); // 手动清空

  // 退出后台的时候，记录时间
  wx.setStorageSync('outTime', +new Date());

  // 发出关闭包事件
  pt.tracking('closeMiNi');

  /**
   * @backSendTimer 退出后台的时候产生计时器，发送数据
   * 当点击小程序的关闭按钮时候，js只存活6s；
   */
  var _b = 1;
  backSendTimer = setInterval(function() {
    _b++;
    if (_b === 6) {
      _b = 1;
    } else {
      // 小程序退到后台后，先发送所有的数据
      if (wx.getStorageSync('pt_temp_data') !== '' && wx.getStorageSync('pt_temp_data') !== null) {
        // console.log('必须走这个');
        wx.getNetworkType({
          success: function(res) {
            var _network = res.networkType;
            // 无网络先把包存起来
            if (_network === 'none') {
              // wx.setStorageSync('pt_temp_data', data);
            } else {
              // 有网络
              // 先判断有没有存储
              pt.store.getSystemError(0, '存储超过系统最大值');
              pt.send(wx.getStorageSync('pt_temp_data'));
              _.info.properties.packData = [];
              wx.removeStorageSync('pt_temp_data');
              console.log('立即发送！');
            }
          }
        });
      }
    }
  }, 1000);


};


var p = App;

App = function(t) {
  e(t, "onLaunch", appLaunch);
  e(t, "onShow", appShow);
  e(t, "onHide", appHide);
  p(t);
};

function pageOnunload(n, e) {

}

function pageOnload(t, n) {

  // console.log(11343534);


};



function pageShare(t, n) {
  var router = typeof this['__route__'] === 'string' ? this['__route__'] : 'route值没有取到';

  console.log('res:' + JSON.stringify(t));
  console.log('n:' + JSON.stringify(n));

  // if 'path' is existence
  if ('path' in t[1]) {
    _.each(t[1], function(a, i) {
      console.log('i:' + i);
      if (i === 'path') {
        t[1].path = t[1].path.indexOf('?') > -1 ? t[1].path + '&pt_ref=' + pt.store.getDistinctId() : t[1].path + '?pt_ref=' + pt.store.getDistinctId();
      }
    });
  } else {
    t[1].path = router + '?pt_ref=' + pt.store.getDistinctId();
  }

  var g = t[1];
  if (typeof t[1]['success'] === 'undefined') {
    t[1]['success'] = function(t) {}
  }
  if (typeof t[1]['fail'] === 'undefined') {
    t[1]['fail'] = function(t) {}
  }
  var _w = t[1]['fail'],
    _y = t[1]['success'];
  t[1]['success'] = function(t) {

    console.log('tt:' + JSON.stringify(t));

    pt.share('ptShare', { path: router });

    _y(t);

  };
  // t[1]["fail"] = function(t) {
  //   w(t)
  // };
  // return t[1];



  // wx.getShareInfo({
  //   shareTicket: t.shareTickets[0],
  //   success: function(res) {
  //     console.log('sdk数据：encryptedData:' + res.encryptedData);
  //     console.log('iv:' + res.iv);
  //   },
  //   complete: function(res) {
  //     console.log('成功转发:' + JSON.stringify(res));
  //   }
  // });

};


function pageOnshow(t, n) {
  var router = typeof this['__route__'] === 'string' ? this['__route__'] : 'route值没有取到';

  console.log('我是pageshow');

  Page_ID = router;
  if (pt.para.onshow) {
    pt.para.onshow(pt, router, this);
  } else {

    setTimeout(function() {
      pt.track('pv', {
        // pageId: router
      });
    }, 100);


    // 页面停留时长
    if (wx.getStorageSync('pt_page_time') !== '') {
      if (wx.getStorageSync('pt_page_time').ptPage !== Page_ID) {
        var _leveTime = new Date().getTime() - wx.getStorageSync('pt_page_time').ptTime,
          _page = wx.getStorageSync('pt_page_time').ptPage;

        setTimeout(function() {
          pt.getPageTime(Math.floor(_leveTime / 1000), _page);

          var _ptTimePage = {
            ptTime: new Date().getTime(),
            ptPage: Page_ID
          };
          wx.setStorageSync('pt_page_time', _ptTimePage);
        }, 100);
      };
    } else {
      var _ptTimePage = {
        ptTime: new Date().getTime(),
        ptPage: Page_ID
      };
      wx.setStorageSync('pt_page_time', _ptTimePage);
    }
  }


};

// var v = Page;
// Page = function(t) {
//   e(t, 'onLoad', pageOnload);
//   e(t, 'onUnload', pageOnunload);
//   e(t, 'onShow', pageOnshow);
//   e(t, 'onHide', pageOnunload);
//   // e(t, 'onShareAppMessage', pageShare);
//   if (typeof t["onShareAppMessage"] != "undefined") {
//     e(t, "onShareAppMessage", pageShare)
//   }
//   v(t);
// }


function c(t, a, e) {
  if (t[a]) {
    var s = t[a];
    t[a] = function(t) {
      var n = s.call(this, t);
      e.call(this, [t, n], a);
      return n
    }
  } else {
    t[a] = function(t) {
      return e.call(this, t, a)
    }
  }
}

var v = Page;
Page = function(t) {
  c(t, 'onLoad', pageOnload);
  c(t, 'onUnload', pageOnunload);
  c(t, 'onShow', pageOnshow);
  c(t, 'onHide', pageOnunload);
  // c(t, 'onShareAppMessage', pageShare);
  if (typeof t["onShareAppMessage"] != "undefined") {
    c(t, "onShareAppMessage", pageShare)
  }
  v(t);
}

// module.exports = pt;
export default pt;