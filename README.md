
小程序SDK版本更新机制：

PTMIND小程序版本采用Semver风格（x.x.x）

如：1.10.9

主版本号为零（0.X.X）标示处于开发初始阶段，一切都可能随时被改变；

本小程序SDK以0.1.0为初始版本；


小程序sdk对外发布流程

（1）将ptskd.js开发版本的头部代码：

```
var pt = {
  'para': require('./ptsdk-conf.js').default,
  'version': '1.0.0'
};
```
修改为：

```
var pt = {
  'para': ''
  'version': '1.0.0'
};
```

（2）请将开发版本ptsdk.js拷贝到
https://gitlab.com/zhaoHuang/publish-mini-program-sdk项目中的/public/scripts/ptMini 路径中

（3）执行gulp build 代码压缩

（4）代码压缩到dist/scripts/ptMini,然后
将代码中的

```
var pt = {
  'para': ''
  'version': '1.0.0'
};
```
修改为：

```
var pt = {
  'para': require('./ptsdk-conf.js').default,
  'version': '1.0.0'
};
```

### 一：标准开发框架sdk接入

（1）在app.js头部引入

```
var pt = require('./utils/ptsdk.js');
```
（2）wepy开发框架sdk接入

在项目的src文件中新建utils文件夹，将sdk源码拷贝到该目录;在src文件夹找到app.wpy文件，将sdk引入

```
require('./utils/ptsdk.js')
```
（3）mpvue开发框架sdk接入

在项目的src文件中新建utils文件夹，将sdk源码拷贝到该目录；

在项目main.js中引入

```
import './utils/ptsdk'
```



至此，完成，可交付用户使用！